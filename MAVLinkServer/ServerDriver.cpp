#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include "Practical.h"
#include <queue>
#include "mavlink.h"
#include <string>
#include <vector>

std::vector<char> encryptData(std::vector<char> rawData, std::vector<char> key) {
    std::vector<char> encryptedData({});
    
    for(int index = 0; index < rawData.size(); index++) {
        if (rawData[index] != '\n')
            encryptedData.push_back((char)rawData[index] ^ key[index % key.size()]);
        else
            encryptedData.push_back(rawData[index]);
    }
    
    return encryptedData;
}

int main(int argc, char *argv[]) {
    
    if (argc != 2) // Test for correct number of arguments
        DieWithUserMessage("Parameter(s)", "<Server Port/Service>");
    
    char *service = argv[1]; // First arg:  local port/service
    
    // Queues to send to different modules
    std::queue<std::string> messagesToGUIModule;
    std::queue<std::string> messagesToMissionExecutiveModule;
    
    // Construct the server address structure
    struct addrinfo addrCriteria;                   // Criteria for address
    memset(&addrCriteria, 0, sizeof(addrCriteria)); // Zero out structure
    addrCriteria.ai_family = AF_UNSPEC;             // Any address family
    addrCriteria.ai_flags = AI_PASSIVE;             // Accept on any address/port
    addrCriteria.ai_socktype = SOCK_DGRAM;          // Only datagram socket
    addrCriteria.ai_protocol = IPPROTO_UDP;         // Only UDP socket
    
    struct addrinfo *servAddr; // List of server addresses
    int rtnVal = getaddrinfo(NULL, service, &addrCriteria, &servAddr);
    if (rtnVal != 0)
        DieWithUserMessage("getaddrinfo() failed", gai_strerror(rtnVal));
    
    // Create socket for incoming connections
    int sock = socket(servAddr->ai_family, servAddr->ai_socktype, servAddr->ai_protocol);
    if (sock < 0)
        DieWithSystemMessage("socket() failed");
    
    // Bind to the local address
    if (bind(sock, servAddr->ai_addr, servAddr->ai_addrlen) < 0)
        DieWithSystemMessage("bind() failed");
    
    // Free address list allocated by getaddrinfo()
    freeaddrinfo(servAddr);
    
    for (;;) { // Run forever
        struct sockaddr_storage clntAddr; // Client address
        // Set Length of client address structure (in-out parameter)
        socklen_t clntAddrLen = sizeof(clntAddr);
        
        // Block until receive message from a client
        char buffer[MAXSTRINGLENGTH]; // I/O buffer
        // Size of received message
        ssize_t numBytesRcvd = recvfrom(sock, buffer, MAXSTRINGLENGTH, 0, (struct sockaddr*)&clntAddr, &clntAddrLen);
        
        if (numBytesRcvd < 0)
            DieWithSystemMessage("recvfrom() failed");
        
        fputs("Handling client ", stdout);
        PrintSocketAddress((struct sockaddr *) &clntAddr, stdout);
        fputc('\n', stdout);
        
        // Something received - print out all bytes and parse packet
        mavlink_status_t status;
        mavlink_message_t msg;
        
        printf("Bytes Received: %d\nDatagram: ", (int)numBytesRcvd);
        for (int i = 0; i < numBytesRcvd; ++i)
        {
            unsigned int temp = 0;
            temp = buffer[i];
            printf("%02x ", (unsigned char)temp);
            if (mavlink_parse_char(MAVLINK_COMM_0, buffer[i], &msg, &status))
            {
                // Packet received
                printf("\nReceived packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
                switch (msg.msgid) {
                    case 0:
                        messagesToMissionExecutiveModule.push(std::string(buffer));
                        printf("messagesToMissionExecutiveModule size: %lu\n", messagesToMissionExecutiveModule.size());
                        break;
                    case 247:
                        messagesToGUIModule.push(std::string(buffer));
                        printf("messagesToGUIModule size: %lu\n", messagesToGUIModule.size());
                        break;
                    default:
                        printf("Message ID not recognized\n");
                        break;
                }
            }
        }
        printf("\n");
        
        // Send received datagram back to the client
        // Last parameter was changed from original implementation due to invalid arg error
        ssize_t numBytesSent = sendto(sock, buffer, numBytesRcvd, 0, (struct sockaddr*)&clntAddr, clntAddrLen);
        if (numBytesSent < 0)
            DieWithSystemMessage("sendto() failed, numBytesSent<0");
        else if (numBytesSent != numBytesRcvd)
            DieWithUserMessage("sendto()", "sent unexpected number of bytes");
    }
    // NOT REACHED
}

