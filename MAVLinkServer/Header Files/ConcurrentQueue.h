#ifndef ConcurrentQueue_h
#define ConcurrentQueue_h

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

template <typename T>
class ConcurrentQueue {
    
    private:
        std::queue<T> queue_;
        std::mutex mutex_;
        std::condition_variable cond_;
        
    public:
    
        int size() {
            std::lock_guard<mutex> locker(mutex_);
            return queue_.size();
        }
        
        T pop() {
            std::unique_lock<std::mutex> mlock(mutex_);
            while (queue_.empty()) {
                cond_.wait(mlock);
            }
            auto item = queue_.front();
            queue_.pop();
            return item;
        }
        
        void pop(T& item) {
            std::unique_lock<std::mutex> mlock(mutex_);
            while (queue_.empty()) {
                cond_.wait(mlock);
            }
            item = queue_.front();
            queue_.pop();
        }
        
        void push(const T& item) {
            std::unique_lock<std::mutex> mlock(mutex_);
            queue_.push(item);
            mlock.unlock();
            cond_.notify_one();
        }
        
        void push(T&& item) {
            std::unique_lock<std::mutex> mlock(mutex_);
            queue_.push(std::move(item));
            mlock.unlock();
            cond_.notify_one();
        }
};

#endif /* ConcurrentQueue_h */
